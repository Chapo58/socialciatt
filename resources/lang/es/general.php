<?php

return [
	'validation_error'			=>	'Error de validacion!',
	'fb_account_not_found'		=>	'Error! Ceunta de FB con el ID: :id No encontrado!',

	'Account deactivated'		=>	'Cuenta desactivada',
	'account_deactivated2'		=>	'Cuenta desactivada por el administrador!',

	'Account expired'			=>	'Cuenta caducada',
	'account_expired2'			=>	'Su cuenta ha caducado!',
	'Expiry date'				=>	'Caduca el',

	'Permission error'			=>	'Error de permisos',

	'maintenance_mode_title'	=>	'Sitio en construccion!',
	'maintenance_mode_text'		=>	'Sitio en construccion!!!',

	'Notifications'				=>	'Notificationes',

	'Add'						=>	'Agregar',
	'Close'						=>	'Cerrar',
	'Create'					=>	'Crear',
	'Name'						=>	'Nombre',

	'Add Facebook account'		=>	'Agregar cuenta de Facebook',
	'Settings'					=>	'Opciones',
	'Logout'					=>	'Salir',
	'you_have_n_notifications'	=>	'Tienes :count notificaciones',

	'All notifications'			=>	'Todas las notificaciones',

	'Add post'					=>	'Agegar post',
	'Publishing'				=>	'Publicando',
	'Insights'					=>	'Estadisticas',
	'Accounts'					=>	'Cuentas',
	'all_rights_reserved'		=>	'© 2018 AutoPost.  Todos los derechos reservados',


	'Upgrade Account'			=>	'Mejorar cuenta',
	'Payment list'				=>	'Lista de pagos',
	'Payment date'				=>	'Fecha de pagos',
	'Amount'					=>	'Monto',
	'Plan'						=>	'Plan',
	'Package'					=>	'Paquete',
	'Payment method'			=>	'Metodo de pago',
	'Payment cycle'				=>	'Ciclo de pago',
	'Status'					=>	'Estado',

	'Monthly'					=>	'Mensual',
	'Annual'					=>	'Anual',

	'PayPal'					=>	'PayPal',
	'STRIPE'					=>	'STRIPE',

	'One time'					=>	'Una vez',
	'Recurring'					=>	'Periodico',

	'Not Approved'				=>	'No Aprobado',
	'Not paid'					=>	'Sin pagar',
	'Paid'						=>	'Pagado',
	'Cancelled'					=>	'Cancelado',
	'Subscribed'				=>	'Suscripto',
	'Subscription cancelled'	=>	'Suscripcion cancelada',

	'Cancel subscription'		=>	'Cancelar suscripcion',

];
