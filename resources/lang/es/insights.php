<?php

return [
	'title'					=>	'Estadisticas',
	'publishing'			=>	'Publicando',
	'monthly'				=>	'Mensualmente',
	'dayly'					=>	'Diariamente',
	'my_fb_accounts'		=>	'Mis cuentas de Facebook',
	'my_data_results'		=>	'Mis resultados',
	'my_account'			=>	'Mi cuenta',
	'posts_per_day'			=>	'Posts por dia',
	'facebook_accounts'		=>	'Cuentas de facebook',
	'package'				=>	'Paquete',
	'account_expiry_in'		=>	'La cuenta caduca en',
	'upload_images'			=>	'Subir imagenes',
	'upload_videos'			=>	'Subir videos',
	'upload_usage'			=>	'Subir uso',
	'total_fb_account'		=>	'Cuentas de Facebook totales',
	'total_groups'			=>	'Grupos Totales',
	'total_pages'			=>	'Paginas totales',
	'schedules'				=>	'Programados',
	'saved_posts'			=>	'Posts guardados',
	'Success'				=>	'Exito',
	'Fail'					=>	'Fallo'
];
