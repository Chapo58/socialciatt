<?php

return [
	'all_posts'							=>	'Todos los posts',
	'scheduled_posts'					=>	'Posts programados',
	'schedule_logs'						=>	'Registros de programados',
	'saved_posts'						=>	'Posts guardados',
	// all posts
	'all.title'							=>	'Todos los posts',
	'all.open_calendar'					=>	'Abrir calendario',
	'all.saved_post'					=>	'Post guardado',
	'all.scheduled_post'				=>	'Post programado',
	// saved posts
	'saved.title'						=>	'Posts guardados',
	'saved.select_all'					=>	'Seleccionar todo',
	'saved.delete'						=>	'Eliminar',
	'saved.new_post'					=>	'Nuevo post',
	'saved.delete_confirmation'			=>	'Eliminar confirmacion',
	'saved.are_you_sure_to_delete'		=>	'¿Seguro que quieres borrar los posts seleccionados?',
	// scheduled posts
	'scheduled.title'					=>	'Posts programados',
	'scheduled.delete'					=>	'Eliminar',
	'scheduled.next_posting_time'		=>	'Siguiente tiempo de publicacion',
	'scheduled.post_interval'			=>	'Intervalo de posteo',
	'scheduled.post'					=>	'Post',
	'scheduled.fb_app'					=>	'Facebook APP',
	'scheduled.fb_account'				=>	'Cuenta de facebook',
	'scheduled.status'					=>	'Estado',
	'scheduled.hours'					=>	'horas',
	'scheduled.minutes'					=>	'minutos',
	'scheduled.completed'				=>	'Completado',
	'scheduled.paused'					=>	'Pausado',
	'scheduled.in_progress'				=>	'En progreso',
	'scheduled.auto_pause'				=>	'Auto pausar despues de: :count posts',
	'scheduled.resume_after'			=>	'Continuar despues:',
	'scheduled.edit'					=>	'Editar',
	'scheduled.view_log'				=>	'Ver log',
	'scheduled.delete_confirmation'		=>	'Eliminar confirmacion',
	'scheduled.are_you_sure_to_delete'	=>	'¿Seguro que quieres borrar los posts seleccionados??',
	// schedule logs
	'logs.title'						=>	'Logs programados',
	'logs.clear_log'					=>	'Limpiar log',
	'logs.published_on'					=>	'Publicado el',
	'logs.node'							=>	'Nodo',
	'logs.node_type'					=>	'Tipo de nodo',
	'logs.post_details'					=>	'Detalles del post',
	'logs.view_post'					=>	'Ver post',
	'logs.no_records_available'			=>	'No hay registros disponibles',
	'logs.delete_confirmation'			=>	'Eliminar confirmacion',
	'logs.are_you_sure_to_delete'		=>	'Seguro que quieres borrar los logs?'
];
