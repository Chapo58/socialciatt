<?php

return [

	'my Profile'												=>	'Perfil',
	'General settings'											=>	'Opciones generales',
	'Publish settings'											=>	'Opciones de publicaciones',
	'Facebook accounts'											=>	'Cuentas de Facebook',
	'Facebook Apps'												=>	'Apps de Facebook',
	'App settings'												=>	'Opciones de la aplicacion',
	'Roles'														=>	'Paquetes',
	'Paypal'													=>	'Paypal',
	'Stripe'													=>	'Stripe',

	'form_error'												=>	'Por favor llene el formulario correctamente!',
	'saved_successfull'											=>	'Guardado correctamente!',

	// Fb Account
	'fb_accounts.title'											=>	'Opciones / Cuentas de Facebook',
	'fb_accounts.FACEBOOK'										=>	'FACEBOOK',
	'fb_accounts.htc'											=>	'htc',
	'fb_accounts.Username'										=>	'Usuario',
	'fb_accounts.Password'										=>	'Contraseña',
	'fb_accounts.ADD FACEBOOK ACCOUNT'							=>	'Agregar cuenta de Facebook',
	'fb_accounts.How to!'										=>	'Como?!',
	'fb_accounts.Access token Here'								=>	'Token de acceso',
	'fb_accounts.Authorize and copy App URL'					=>	'Autorizar y copiar URL de la APP',
	'fb_accounts.password_will_not_be_stored'					=>	'La contraseña de su cuenta de Facebook NO se almacenará, solo la utilizamos para generar un token de Facebook',
	'fb_accounts.Make sure to set the visibility to public'		=>	'Asegúrate de configurar la visibilidad en público.',
	'fb_accounts.Load my groups'								=>	'Cargar grupos',
	'fb_accounts.Load my pages'									=>	'Cargar paginas',
	'fb_accounts.Load my ownpages'								=>	'Cargar paginas propias',
	'fb_accounts.Maximum groups to import'						=>	'Grupos maximos',
	'fb_accounts.Maximum pages to import'						=>	'Paginas maximas',
	'fb_accounts.Save changes'									=>	'Guardar cambios',
	'fb_accounts.Full name'										=>	'Nombre completo',
	'fb_accounts.ID'											=>	'ID',
	'fb_accounts.E-mail'										=>	'E-mail',
	'fb_accounts.Delete confirmation'							=>	'Eliminar confirmacion',
	'fb_accounts.Are you sure you want to delete the account?'	=>	'¿Estás seguro de que quieres eliminar la cuenta?',
	'fb_accounts.Update confirmation'							=>	'Confirmacion de actualizacion',
	'fb_accounts.Are you sure you want to update the account?'	=>	'¿Estás seguro de que quieres actualizar la cuenta?',

	// my profile
	'profile.title'												=>	'Opciones de perfil',
	'profile.Username'											=>	'Usuario(No puede ser modificado)',
	'profile.First name'										=>	'Nombre',
	'profile.E-mail'											=>	'E-mail',
	'profile.Do you want to change password?'					=>	'Desea cambiar la contraseña?',
	'profile.Change password'									=>	'Cambiar contraseña',
	'profile.Last name'											=>	'Apellido',
	'profile.Facebook user id'									=>	'ID usuario de Facebook',
	'profile.Current password'									=>	'Contraseña actual',
	'profile.New password'										=>	'Nueva contraseña',
	'profile.Repeat New password'								=>	'Repetir nueva contraseña',
	'profile.Change passwordSave'								=>	'Cambiar contraseña',
	'profile.password_error'									=>	'Las contraseñas no coinciden!',
	'profile.validation_error1'									=>	'No has rellenado completamente la informacion.!',

	// general
	'general.title'												=>	'Opciones generales',
	'general.Records per page'									=>	'Registros por página',
	'general.Timezone | Current'								=>	'Zona horaria',
	'general.Language'											=>	'Lenguaje',
	'general.Save changes'										=>	'Guardar cambios',

	// publish
	'publish.title'												=>	'Opciones de publicaciones',
	'publish.Show Open groups only'								=>	'Ver solo grupos abiertos',
	'publish.Unique post'										=>	'Post unico',
	'publish.Unique link'										=>	'Link unico',
	'publish.Enable link customization'							=>	'Habilitar la personalización de Links',
	'publish.Post interval (In seconds)'						=>	'Intervalo de posteo (en segundos)',
	'publish.Facebook app'										=>	'Facebook app',
	'publish.Save changes'										=>	'Guardar cambios',

	// facebook apps
	'fb_apps.fb_apps.title'										=>	'Opciones de facebook app',
	'fb_apps.warning1'											=>	'Por favor, asegúrese de iniciar sesión en Facebook como ":name" antes de autorizar la app.',
	'fb_apps.Facebook app ID'									=>	'Facebook app ID',
	'fb_apps.Facebook app Secret'								=>	'Facebook app Secret',
	'fb_apps.Facebook app authenticate link'					=>	'Facebook app authenticate link',
	'fb_apps.Make this app public'								=>	'Hacer publica la app',
	'fb_apps.ADD'												=>	'Agregar',
	'fb_apps.App name'											=>	'Nombre de la APP',
	'fb_apps.Expires on'										=>	'Caduca el',
	'fb_apps.Never'												=>	'Nunca',
	'fb_apps.---------'											=>	'---------',
	'fb_apps.Delete'											=>	'Eliminar',
	'fb_apps.Deauthenticate'									=>	'Quitar',
	'fb_apps.Authenticate'										=>	'Autenticar',
	'fb_apps.DeauthenticateConfirmation'						=>	'Quitar confirmacion',
	'fb_apps.DeauthenticateConfirmation2'						=>	'Seguro que quiere quitar la app',
	'fb_apps.Delete confirmation'								=>	'Eliminar confirmacion',
	'fb_apps.Are you sure you want to delete the app?'			=>	'Seguro que quiere eliminar la app?',

	// roles
	'roles.title'												=>	'Opciones / Roles',
	'roles.Name'												=>	'Nombre',
	'roles.Delete confirmation'									=>	'Eliminar confirmacion',
	'roles.Are you sure you want to delete the role?'			=>	'Seguro que quiere eliminar el rol?',
	'roles.Cancel'												=>	'Cancelar',
	'roles.Delete'												=>	'Eliminar',
	'roles.ValidationError'										=>	'No has rellenado completamente la informacion.!',
	'roles.Role name'											=>	'Nombre del rol',
	'roles.Maximum posts per day'								=>	'Posts maximos por dia',
	'roles.Maximum Facebook accounts'							=>	'Cuentas de facebook maximas',
	'roles.Account expire(In days)'								=>	'Cuenta caduca (en dias)',
	'roles.Upload Videos'										=>	'Subir Videos',
	'roles.Upload Images'										=>	'Subir Imagenes',
	'roles.Max Upload(MB)'										=>	'Maximo tamaño de archivo(MB)',
	'roles.Save'												=>	'Guardar',
	'roles.Max Posts'											=>	'Post maximos',
	'roles.Max Facebook account'								=>	'Cuentas de facebook maximas',
	'roles.Account expiry'										=>	'Cuenta caduca',
	'roles.Can upload videos'									=>	'Puede subir videos',
	'roles.Can upload images'									=>	'Puede subir imagenes',
	'roles.Max Upload'											=>	'Subida maxima',
	'roles.Monthly price (USD)'									=>	'Precio mensual (USD)',
	'roles.Annual price (USD)'									=>	'Precio anual (USD)',

	// app
	'app.General'												=>	'General',
	'app.Publish settings'										=>	'Opciones de publicacion',
	'app.Theme settings'										=>	'Opciones del tema',
	'app.Ads Settings'											=>	'Opciones de Anuncios',
	'app.Social login'											=>	'Social login',
	'app.Advanced settings'										=>	'Opciones avanzadas',
	'app.Mail settings'											=>	'Opciones de email',

	// app / general
	'app.general.title'											=>	'Opciones / App / General',
	'app.general.Site name'										=>	'Nombre del sitio',
	'app.general.New users can register'						=>	'Permitir nuevos registros',
	'app.general.New users must confirm their email address'	=>	'Los nuevos usuarios deben confirmar su correo',
	'app.general.New users accounts must be activated by an admin'	=>	'Las cuentas nuevas deben ser activadas por el administrador',
	'app.general.New users Default role'						=>	'Rol por defecto para usuarios nuevos',
	'app.general.New users Default timezone'					=>	'Zona por defecto para usuarios nuevos',
	'app.general.Default language'								=>	'Idioma por defecto',
	'app.general.Date format'									=>	'Formato de fecha',
	'app.general.Enable Lite mode for nodes table'				=>	'Habilitar el modo Lite para la tabla de nodos',
	'app.general.Save changes'									=>	'Guardar cambios',

	// app / publish
	'app.publish.title'											=>	'Opciones / App / Publicaciones',
	'app.publish.Minimum interval for immediate posting (In seconds)'		=>	'Intervalo mínimo para publicación inmediata (en segundos)',
	'app.publish.Minimum interval on schedule post (in minutes)'=>	'Intervalo minimo para post programados (en minutos)',
	'app.publish.Enable Schedule Random interval'				=>	'Habilitar intervalos aleatorios para post programados',
	'app.publish.Off'											=>	'Apagar',
	'app.publish.Minute'										=>	'Minutos',
	'app.publish.Instant post Random interval'					=>	'Intervalo aleatorio para publicacion instantanea ',
	'app.publish.Sec'											=>	'Segundos',
	'app.publish.Enable instant post'							=>	'Habilitar post instantaneo',
	'app.publish.Enable sale post type'							=>	'Habilitar post de ventas',
	'app.publish.Enable link customization'						=>	'Habilitar personalización de links',
	'app.publish.Save changes'									=>	'Guardar',

	// app / theme
	'app.theme.title'											=>	'Opciones / App / Tema',
	'app.theme.Site logo (Recommended size 100x32px)'			=>	'Logo (Recomendado 100x32px)',
	'app.theme.Site logo (Recommended size 50x50 px )'			=>	'Logo (Recomendado 50x50 px )',
	'app.theme.Site large logo (Recommended size 300x100 px )'	=>	'Logo grande (Recomendado 300x100 px )',
	'app.theme.Site favicon'									=>	'Favicon',
	'app.theme.Site meta description'							=>	'META descripcion',
	'app.theme.Theme color'										=>	'Color del tema',
	'app.theme.Theme links color'								=>	'Color de los links',
	'app.theme.Theme background image Public pages (Login page, Register page ..)'=>	'Imagen de fondo (Pagina de ingreso, Pagina de registro..)',
	'app.theme.Theme background color Public pages (Login page, Register page ..)'=>	'Color de fondo (Pagina de ingreso, Pagina de registro..)',
	'app.theme.Custom CSS'										=>	'CSS personalizado',
	'app.theme.Footer text (Support HTML tags)'					=>	'Texto de pie de pagina (Soporta tags HTML)',
	'app.theme.Save changes'									=>	'Guardar cambios',

	// app / ads
	'app.ads.title'												=>	'Opciones / App / Anuncios',
	'app.ads.Ads banner ( Responsive ADS code )'				=>	'Banner de anuncios ( Codigos responsivos de anuncios )',
	'app.ads.Display Ads on public pages'						=>	'Mostrar anuncios en paginas',
	'app.ads.Show ADS to'										=>	'Mostrar anuncios a',
	'app.ads.Save changes'										=>	'Guardar cambios',

	// app / social login
	'app.social_login.title'									=>	'Opciones / App / Social login',
	'app.social_login.Facebook app id'							=>	'Facebook app id',
	'app.social_login.Facebook App secret'						=>	'Facebook App secret',
	'app.social_login.Save changes'								=>	'Guardar cambios',

	// app / advanced
	'app.advanced.title'										=>	'Opciones / App / Avanzadas',
	'app.advanced.Head javascript'								=>	'Cabecera javascript',
	'app.advanced.Footer javascript'							=>	'Pie de pagina javascript',
	'app.advanced.Active Maintenance Mode'						=>	'Activar modo mantenimiento',
	'app.advanced.Save changes'									=>	'Guardar cambios',

	// app / mail
	'app.mail.title'											=>	'Opciones / App / Mail',
	'app.mail.Mail protocol'									=>	'Protocolo Mail',
	'app.mail.Mail'												=>	'Mail',
	'app.mail.SMTP'												=>	'SMTP',
	'app.mail.SMTP host'										=>	'SMTP host',
	'app.mail.SMTP user'										=>	'SMTP user',
	'app.mail.SMTP pass'										=>	'SMTP pass',
	'app.mail.SMTP port'										=>	'SMTP port',
	'app.mail.SMTP encryption'									=>	'SMTP encryption',
	'app.mail.Save changes'										=>	'Guardar cambios',

	// paypal settings
	'paypal.title'												=>	'Paypal',
	'paypal.Environment'										=>	'Environment',
	'paypal.Live'												=>	'Live',
	'paypal.Test mode (sandbox)'								=>	'Modo de prueba (sandbox)',
	'paypal.PayPal Client ID'									=>	'PayPal Client ID',
	'paypal.PayPal Client Secret'								=>	'PayPal Client Secret',

	// stripe settings
	'stripe.title'												=>	'Stripe',
	'stripe.Environment'										=>	'Environment',
	'stripe.Live'												=>	'Live',
	'stripe.Test mode (sandbox)'								=>	'Modo de pruebae (sandbox)',
	'stripe.Publishable Key'									=>	'Publishable Key',
	'stripe.Secret Key'											=>	'Secret Key',
	'stripe.Signing Secret Key for Webhook'					=>	'Cargando llave secreta para Webhook',





































];
